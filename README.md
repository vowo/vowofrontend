# vowo.io Frontend

[![pipeline status](https://gitlab.com/vowo/vowofrontend/badges/master/pipeline.svg)](https://gitlab.com/vowo/vowofrontend/commits/master)

Frontent for vowo.io, developed in React.js

## Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app)
It uses Sass (with .scss). The styles are loaded at the template level with `node-sass-chokidar` css preprocessor

Dependencies are handled by **npm**.

### Basic usage

```bash
$ npm i # to install dependencies
# dev server  with hot reload at http://localhost:3000
$ npm start
$ npm run build  #to run a dev build
```

Navigate to [http://localhost:3000](http://localhost:3000). The app will automatically reload if you change any of the source files.

### Build

Run `build` to build the project. The build artifacts will be stored in the `build/` directory.

```bash
# build for production with minification
$ npm run build

# or run the docker build locally
$ sh ./build-docker-local.sh

# test it
$ docker run -it -p 80:80 --rm  vowofrontend:latest
```

## Project Structure

Within the download you'll find the following directories and files, logically grouping common assets and providing both compiled and minified variations. You'll see something like this:

```
vowo.io
├── public/          #static files
│   ├── assets/      #assets
│   └── index.html   #html temlpate
│
├── src/             #project root
│   ├── containers/  #container source
│   ├── scss/        #user scss/css source
│   ├── views/       #views source
│   ├── App.js
│   ├── App.test.js
│   ├── index.js
│   ├── _nav.js      #sidebar config
│   └── routes.js    #routes config
│
└── package.json
```
