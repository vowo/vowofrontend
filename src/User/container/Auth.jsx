import React, { Component } from "react";
import LoginButton from "../components/LoginButton";
import { loggedIn, login, logout } from "../AuthService";
import { ErrorModal } from "../components/ErrorModal";
import UserNav from "../components/UserNav";
import { fetch } from "../AuthService";

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false,
      loginError: false
    };

    this.onLoginSuccess = this.onLoginSuccess.bind(this);
    this.onLoginError = this.onLoginError.bind(this);
    this.onLogoutComplete = this.onLogoutComplete.bind(this);
    this.toggleWarning = this.toggleWarning.bind(this);
  }

  componentDidMount() {
    this.setState({ isLoggedIn: loggedIn() });
  }

  onLoginSuccess(res) {
    if (res) {
      this.props.toggleLogin();
      login(res.Zi.id_token);
      this.setState({ isLoggedIn: true });
      fetch("https://vowo-vowobackend.vowo.io/me/info").then(info =>
        console.log("BackendUserInfo", info)
      );
    }
  }

  onLoginError() {
    this.setState({ loginError: true });
  }

  onLogoutComplete() {
    this.props.toggleLogin();
    logout();
    this.setState({ isLoggedIn: false });
  }

  toggleWarning() {
    this.setState(state => {
      return { loginError: !state.loginError };
    });
  }

  render() {
    const authButton = this.state.isLoggedIn ? (
      <UserNav onLogoutComplete={this.onLogoutComplete} />
    ) : (
      <LoginButton
        onLoginSuccess={this.onLoginSuccess}
        onLoginError={this.onLoginError}
      />
    );
    return (
      <React.Fragment>
        {authButton}
        <ErrorModal
          display={this.state.loginError}
          toggleFunction={this.toggleWarning}
        />
      </React.Fragment>
    );
  }
}

export default Auth;
