import React, { Component } from "react";
import { Bar } from "react-chartjs-2";
import {
  Card,
  CardBody,
  CardFooter,
  CardTitle,
  Col,
  Row
} from "reactstrap";
import { mainChart, mainChartOpts } from "../FootprintData";

class Footprint extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6" lg="4">
            <Card className="text-white bg-success">
              <CardBody className="pb-0">
                <div className="text-value">
                  5.364 g <i className="icon-drop" />
                </div>
                <div>CO2 Bilanz Dezember</div>
              </CardBody>
              <div className="chart-wrapper mx-3" style={{ height: "40px" }}/>
            </Card>
          </Col>
          <Col xs="12" sm="6" lg="4">
            <Card className="text-white bg-warning">
              <CardBody className="pb-0">
                <div className="text-value">
                  100 km<i className="icon-location-pin" />
                  ...
                  <i className="icon-location-pin" />
                </div>
                <div>LKW Distanz</div>
              </CardBody>
              <div className="chart-wrapper" style={{ height: "40px" }} />
            </Card>
          </Col>
          {/*<Col xs="12" sm="6" lg="4">*/}
            {/*<Card className="text-white bg-danger">*/}
              {/*<CardBody className="pb-1">*/}
                {/*<div className="text-value">*/}
                  {/*2'200km <i className="icon-paper-plane" />*/}
                {/*</div>*/}
                {/*<div>Flugzeug Distanz Heute</div>*/}
              {/*</CardBody>*/}
              {/*<div className="chart-wrapper mx-3" style={{ height: "70px" }}>*/}
                {/*<Bar*/}
                  {/*data={cardChartData4}*/}
                  {/*options={cardChartOpts4}*/}
                  {/*height={70}*/}
                {/*/>*/}
              {/*</div>*/}
            {/*</Card>*/}
          {/*</Col>*/}
        </Row>
        <Row>
          <Col>
            <Card>
              <CardBody>
                <Row>
                  <Col sm="5">
                    <CardTitle className="mb-0">Emissionen</CardTitle>
                    <div className="small text-muted">Dezember 2018</div>
                  </Col>
                </Row>
                <div
                  className="chart-wrapper"
                  style={{ height: 300 + "px", marginTop: 40 + "px" }}
                >
                  <Bar data={mainChart} options={mainChartOpts} height={300} />
                </div>
              </CardBody>
              <CardFooter>
                <Row className="text-center">
                  <Col sm={12} md className="mb-sm-2 mb-0">
                    <div className="text-muted">Einkäufe</div>
                    <strong>98 Einkäufe (+ 3.45%)</strong>
                  </Col>
                  <Col sm={12} md className="mb-sm-2 mb-0 d-md-down-none">
                    <div className="text-muted">CO2 Neutrale Produkte</div>
                    <strong>(- 3.45%)</strong>
                  </Col>
                  <Col sm={12} md className="mb-sm-2 mb-0">
                    <div className="text-muted">
                      davon CO2 Belastende Produkte
                    </div>
                    <strong>(5.78%)</strong>
                  </Col>
                </Row>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Footprint;
