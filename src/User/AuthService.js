import decode from "jwt-decode";

export const loggedIn = () => {
  const token = getToken();
  return !!token && !isTokenExpired(token);
};

export const isTokenExpired = (token) => {
  try {
    const decoded = decode(token);
    return decoded.exp < Date.now() / 1000;
  }
  catch (err) {
    return false;
  }
};

export const login = (token) => {
  localStorage.setItem('id_token', token);
};

export const getToken = () => {
  return localStorage.getItem('id_token');
};

export const getImageUrl = () => {
  return decode(getToken()).picture;
};

export const getUserEmail = () => {
  return decode(getToken()).email;
};

export const logout = () => {
  localStorage.removeItem('id_token');
};

export const fetch = (url, options) => {
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };

  if (loggedIn()) {
    headers['Authorization'] = 'Bearer ' + getToken()
  }

  return window.fetch(url, {
    headers,
    ...options
  })
    .then(_checkStatus)
    .then(response => response.json());
};

const _checkStatus = (response) => {
  // raises an error in case response status is not a success
  if (response.status >= 200 && response.status < 300) { // Success status lies between 200 to 300
    return response;
  } else {
    // TODO: better error handling
    const error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
};
