import React from "react";
import { Button } from "reactstrap";
import GoogleLogin from "react-google-login";

const googleClientId = '485821073907-is1243piagsgfftbnm05m9i37uopv2u1.apps.googleusercontent.com';

const LoginButton = ({ onLoginSuccess, onLoginError }) => (
  <GoogleLogin
    clientId={googleClientId}
    render={renderProps => (
      <Button className="mr-4 ml-auto" onClick={renderProps.onClick} color='light'>Login</Button>
    )}
    onSuccess={onLoginSuccess}
    onFailure={onLoginError}
  />
);

export default LoginButton;
