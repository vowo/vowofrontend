import React from "react";
import { AppHeaderDropdown } from "@coreui/react";
import { GoogleLogout } from "react-google-login";
import { DropdownItem, DropdownMenu, DropdownToggle, Nav } from "reactstrap";
import { getImageUrl, getUserEmail } from "../AuthService";

const UserNav = ({ onLogoutComplete }) => (
  <Nav className="ml-auto" navbar>
    <AppHeaderDropdown direction="down">
      <DropdownToggle nav>
        <img
          src={getImageUrl()}
          className="img-avatar"
          alt={getUserEmail()}
        />
      </DropdownToggle>
      <DropdownMenu right style={{ right: "auto" }}>
        <DropdownItem header tag="div" className="text-center">
          <strong>Settings</strong>
        </DropdownItem>
        <DropdownItem>
          <i className="fa fa-user" /> Profil
        </DropdownItem>
        <DropdownItem>
          <i className="fa fa-wrench" /> Einstellungen
        </DropdownItem>
        <GoogleLogout
          render={renderProps => (
            <DropdownItem onClick={renderProps.onClick}>
              <i className="fa fa-lock" />Ausloggen
            </DropdownItem>)}
          onLogoutSuccess={onLogoutComplete}>
        </GoogleLogout>
      </DropdownMenu>
    </AppHeaderDropdown>
  </Nav>
);

export default UserNav;
