import React from "react";
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";

export const ErrorModal = ({display, toggleFunction}) => (
  <Modal isOpen={display} toggle={toggleFunction}
         className={'modal-warning'}>
    <ModalHeader toggle={toggleFunction}>Something went Wrong with the Google Login!</ModalHeader>
    <ModalBody>
      Please excuse us! Something went wrong while trying to Log In with Google. Try again later.
    </ModalBody>
    <ModalFooter>
      <Button color="warning" onClick={toggleFunction}>Ok</Button>
    </ModalFooter>
  </Modal>
);
