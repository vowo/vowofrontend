import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import {
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from '../_nav';
// routes config
import routes from '../routes';
import Footer from './Footer';
import Header from './Header';
import { loggedIn } from "../User/AuthService";

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: loggedIn(),
      navigation: navigation
    };

    this.toggleLogin = this.toggleLogin.bind(this);
  }

  componentDidMount() {
    if(loggedIn()) {
      this.setState({
        navigation: {
          items: [...navigation.items, {
            name: "Mein Fussabdruck",
            url: "/footprint",
            icon: "icon-drop"
          }]
        }
      });
    }
  }

  toggleLogin() {
    this.setState({loggedIn: !this.state.loggedIn})
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.loggedIn !== prevState.loggedIn) {
      if (this.state.loggedIn) {
        this.setState({
          navigation: {
            items: [...navigation.items, {
              name: "Mein Fussabdruck",
              url: "/footprint",
              icon: "icon-drop"
            }]
          }
        });
      } else {
        this.setState({
          navigation: navigation
        })
      }
    }
  }

  render() {
    return (
      <div className="app">
        <AppHeader fixed>
          <Header toggleLogin={this.toggleLogin} />
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarNav navConfig={this.state.navigation} {...this.props} />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes}/>
            <Container fluid>
              <Switch>
                {routes.map((route, idx) => {
                    return route.component ? (<Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => (
                        <route.component {...props} />
                      )} />)
                      : (null);
                  },
                )}
                <Redirect from="/" to="/dashboard" />
              </Switch>
            </Container>
          </main>
        </div>
        <AppFooter>
          <Footer />
        </AppFooter>
      </div>
    );
  }
}

export default Layout;
