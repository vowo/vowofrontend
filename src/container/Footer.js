import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <React.Fragment>
        <span>vowo.io &copy; 2018</span>
        <span className="ml-auto">
          Powered by <a href="https://coreui.io/react">CoreUI for React</a>
        </span>
      </React.Fragment>
    );
  }
}

export default Footer;
