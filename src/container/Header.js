import React, { Component } from "react";
import { AppNavbarBrand, AppSidebarToggler } from "@coreui/react";
import logo from "../assets/img/brand/logo.png";
import sygnet from "../assets/img/brand/sygnet.svg";
import Auth from "../User/container/Auth";

class Header extends Component {
  render () {
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: "CoreUI Logo" }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: "CoreUI Logo" }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg"/>
        <Auth toggleLogin={this.props.toggleLogin}/>
      </React.Fragment>
    );
  }
}

export default Header;
