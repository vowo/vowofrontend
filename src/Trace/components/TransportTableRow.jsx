import React, { Component } from "react";
import {
    FaTruckMoving,
    FaTrain,
    FaShip,
    FaPlane,
    FaBicycle
} from "react-icons/fa";
import { IconContext } from "react-icons";



class TransportTableRow extends Component {

    setIcon(iconName) {
        var icon;
        switch (this.props.iconName) {
            case "truck":
                icon = <FaTruckMoving />;
                break;
            case "train":
                icon = <FaTrain />;
                break;
            case "ship":
                icon = <FaShip />;
                break;
            case "plane":
                icon = <FaPlane />;
                break;
            case "bicycle":
                icon = <FaBicycle />;
                break;
            default:
                icon = <FaTruckMoving />;
                break;
        }
        return icon;
    }

    render() {
        return (
            <tr>

                <td>
                    <IconContext.Provider
                        value={{ color: this.props.color }}
                    >
                        <h4>
                            {this.setIcon(this.props.iconName)}
                        </h4>
                    </IconContext.Provider>
                </td>
                <td className="text-right">{this.props.distance} Km</td>
            </tr>
        );
    }
}

export default TransportTableRow;
