import React  from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { withRouter } from "react-router-dom";

const navigateMyFootprint = (history) => {
  history.push('/footprint')
};

const AddedTraceModal = ({ display, toggleFunction, history }) => (
  <Modal isOpen={display} toggle={toggleFunction} className={"modal-info"}>
    <ModalHeader toggle={toggleFunction}>Proukt zu meinem Fussabdruck hinzugefügt</ModalHeader>
    <ModalBody>
      Das Produkt wurde erfolgreich zu deinem persönlichen Fussabdruck hinzugefügt.
    </ModalBody>
    <ModalFooter>
      <Button color="info" onClick={() => navigateMyFootprint(history)}>
        Zu meinem Fussabdruck
      </Button>
      <Button color="info" onClick={toggleFunction}>
        Schliessen
      </Button>
    </ModalFooter>
  </Modal>
);

export default withRouter(AddedTraceModal);
