import React, { Component } from "react";
import { withRouter } from "react-router-dom";

//https://github.com/zpao/qrcode.react
var QRCode = require("qrcode.react");

const APP_URL = process.env.REACT_APP_URL;

class QrCode extends Component {
  render() {
    return <QRCode value={APP_URL + "#/product/trace/" + this.props.traceId} />;
  }
}

export default QrCode;
