import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import QrCode from "./QrCode";

export const QrCodeModal = ({ display, toggleFunction, traceId }) => (
  <Modal isOpen={display} toggle={toggleFunction} className={"modal-info"}>
    <ModalHeader toggle={toggleFunction}>QR Code</ModalHeader>
    <ModalBody>
      <QrCode traceId={traceId} />
    </ModalBody>
    <ModalFooter>
      <Button color="info" onClick={toggleFunction}>
        Close
      </Button>
    </ModalFooter>
  </Modal>
);
