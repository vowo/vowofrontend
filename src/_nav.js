export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer",
      badge: {
        variant: "success",
        text: "2"
      }
    },
    {
      name: "Produkte",
      url: "/product",
      icon: "icon-drop"
    },
    {
      name: "Produkt Hinzufügen",
      url: "/productScan",
      icon: "icon-basket"
    }
  ]
};
