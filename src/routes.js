import React from "react";
import Loadable from "react-loadable";
import { Redirect } from "react-router-dom";

function Loading() {
  return <div>Loading...</div>;
}

const Breadcrumbs = Loadable({
  loader: () => import("./_examples/Base/Breadcrumbs"),
  loading: Loading
});

const Cards = Loadable({
  loader: () => import("./_examples/Base/Cards"),
  loading: Loading
});

const Carousels = Loadable({
  loader: () => import("./_examples/Base/Carousels"),
  loading: Loading
});

const Collapses = Loadable({
  loader: () => import("./_examples/Base/Collapses"),
  loading: Loading
});

const Dropdowns = Loadable({
  loader: () => import("./_examples/Base/Dropdowns"),
  loading: Loading
});

const Forms = Loadable({
  loader: () => import("./_examples/Base/Forms"),
  loading: Loading
});

const Jumbotrons = Loadable({
  loader: () => import("./_examples/Base/Jumbotrons"),
  loading: Loading
});

const ListGroups = Loadable({
  loader: () => import("./_examples/Base/ListGroups"),
  loading: Loading
});

const Navbars = Loadable({
  loader: () => import("./_examples/Base/Navbars"),
  loading: Loading
});

const Navs = Loadable({
  loader: () => import("./_examples/Base/Navs"),
  loading: Loading
});

const Paginations = Loadable({
  loader: () => import("./_examples/Base/Paginations"),
  loading: Loading
});

const Popovers = Loadable({
  loader: () => import("./_examples/Base/Popovers"),
  loading: Loading
});

const ProgressBar = Loadable({
  loader: () => import("./_examples/Base/ProgressBar"),
  loading: Loading
});

const Switches = Loadable({
  loader: () => import("./_examples/Base/Switches"),
  loading: Loading
});

const Tables = Loadable({
  loader: () => import("./_examples/Base/Tables"),
  loading: Loading
});

const Tabs = Loadable({
  loader: () => import("./_examples/Base/Tabs"),
  loading: Loading
});

const Tooltips = Loadable({
  loader: () => import("./_examples/Base/Tooltips"),
  loading: Loading
});

const BrandButtons = Loadable({
  loader: () => import("./_examples/Buttons/BrandButtons"),
  loading: Loading
});

const ButtonDropdowns = Loadable({
  loader: () => import("./_examples/Buttons/ButtonDropdowns"),
  loading: Loading
});

const ButtonGroups = Loadable({
  loader: () => import("./_examples/Buttons/ButtonGroups"),
  loading: Loading
});

const Buttons = Loadable({
  loader: () => import("./_examples/Buttons/Buttons"),
  loading: Loading
});

const Charts = Loadable({
  loader: () => import("./_examples/Charts"),
  loading: Loading
});

const Dashboard = Loadable({
  loader: () => import("./Dashboard/container/Dashboard"),
  loading: Loading
});

const CoreUIIcons = Loadable({
  loader: () => import("./_examples/Icons/CoreUIIcons"),
  loading: Loading
});

const Flags = Loadable({
  loader: () => import("./_examples/Icons/Flags"),
  loading: Loading
});

const FontAwesome = Loadable({
  loader: () => import("./_examples/Icons/FontAwesome"),
  loading: Loading
});

const SimpleLineIcons = Loadable({
  loader: () => import("./_examples/Icons/SimpleLineIcons"),
  loading: Loading
});

const Alerts = Loadable({
  loader: () => import("./_examples/Notifications/Alerts"),
  loading: Loading
});

const Badges = Loadable({
  loader: () => import("./_examples/Notifications/Badges"),
  loading: Loading
});

const Modals = Loadable({
  loader: () => import("./_examples/Notifications/Modals"),
  loading: Loading
});

const Page404 = Loadable({
  loader: () => import("./common/Page404"),
  loading: Loading
});

const ProductOverview = Loadable({
  loader: () => import("./Product/container/ProductOverview"),
  loading: Loading
});

const Product = Loadable({
  loader: () => import("./Product/container/Product"),
  loading: Loading
});

const ProductDetail = Loadable({
  loader: () => import("./Product/container/ProductDetail"),
  loading: Loading
});

const ProductTrace = Loadable({
  loader: () => import("./Product/container/ProductTrace"),
  loading: Loading
});

const Footprint = Loadable({
  loader: () => import("./User/container/Footprint"),
  loading: Loading
});

const Widgets = Loadable({
  loader: () => import("./_examples/Widgets/Widgets"),
  loading: Loading
});

const Users = Loadable({
  loader: () => import("./_examples/Users/Users"),
  loading: Loading
});

const User = Loadable({
  loader: () => import("./_examples/Users/User"),
  loading: Loading
});

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: "/", exact: true, component: () => <Redirect to="/dashboard" /> },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  //{ path: "/theme", exact: true, name: "Theme", component: Colors },
  {
    path: "/product",
    exact: true,
    name: "Product",
    component: ProductOverview
  },
  {
    path: "/product/trace/:traceId",
    name: "Product Trace",
    component: ProductTrace
  },
  {
    path: "/product/:productId",
    name: "Product Detail",
    component: ProductDetail
  },
  { path: "/productScan", name: "Product", component: Product },
  { path: "/footprint", name: "Footprint", component: Footprint },
  { path: "/base", exact: true, name: "Base", component: Cards },
  { path: "/base/cards", name: "Cards", component: Cards },
  { path: "/base/forms", name: "Forms", component: Forms },
  { path: "/base/switches", name: "Switches", component: Switches },
  { path: "/base/tables", name: "Tables", component: Tables },
  { path: "/base/tabs", name: "Tabs", component: Tabs },
  { path: "/base/breadcrumbs", name: "Breadcrumbs", component: Breadcrumbs },
  { path: "/base/carousels", name: "Carousel", component: Carousels },
  { path: "/base/collapses", name: "Collapse", component: Collapses },
  { path: "/base/dropdowns", name: "Dropdowns", component: Dropdowns },
  { path: "/base/jumbotrons", name: "Jumbotrons", component: Jumbotrons },
  { path: "/base/list-groups", name: "List Groups", component: ListGroups },
  { path: "/base/navbars", name: "Navbars", component: Navbars },
  { path: "/base/navs", name: "Navs", component: Navs },
  { path: "/base/paginations", name: "Paginations", component: Paginations },
  { path: "/base/popovers", name: "Popovers", component: Popovers },
  { path: "/base/progress-bar", name: "Progress Bar", component: ProgressBar },
  { path: "/base/tooltips", name: "Tooltips", component: Tooltips },
  { path: "/buttons", exact: true, name: "Buttons", component: Buttons },
  { path: "/buttons/buttons", name: "Buttons", component: Buttons },
  {
    path: "/buttons/button-dropdowns",
    name: "Button Dropdowns",
    component: ButtonDropdowns
  },
  {
    path: "/buttons/button-groups",
    name: "Button Groups",
    component: ButtonGroups
  },
  {
    path: "/buttons/brand-buttons",
    name: "Brand Buttons",
    component: BrandButtons
  },
  { path: "/icons", exact: true, name: "Icons", component: CoreUIIcons },
  { path: "/icons/coreui-icons", name: "CoreUI Icons", component: CoreUIIcons },
  { path: "/icons/flags", name: "Flags", component: Flags },
  { path: "/icons/font-awesome", name: "Font Awesome", component: FontAwesome },
  {
    path: "/icons/simple-line-icons",
    name: "Simple Line Icons",
    component: SimpleLineIcons
  },
  {
    path: "/notifications",
    exact: true,
    name: "Notifications",
    component: Alerts
  },
  { path: "/notifications/alerts", name: "Alerts", component: Alerts },
  { path: "/notifications/badges", name: "Badges", component: Badges },
  { path: "/notifications/modals", name: "Modals", component: Modals },
  { path: "/widgets", name: "Widgets", component: Widgets },
  { path: "/charts", name: "Charts", component: Charts },
  { path: "/users", exact: true, name: "Users", component: Users },
  { path: "/users/:id", exact: true, name: "User Details", component: User },
  { path: "/404", exact: true, name: "404", component: Page404 },
  { path: "*", exact: true, name: "404", component: Page404 }
];

export default routes;
