import React, { Component } from "react";
import { Card, CardBody, Col, Row, Table, Button } from "reactstrap";
import { withRouter } from "react-router-dom";
import { fetch } from "../../User/AuthService";

import ProductMap from "../components/ProductMap";
import { QrCodeModal } from "../../Trace/components/QrCodeModal";
import TransportTableRow from "../../Trace/components/TransportTableRow";
import AddedTraceModal from "../../Trace/components/AddedTraceModal";

var Loader = require("react-loader");

const API_URL = process.env.REACT_APP_API_URL;
const TRACE_URL = API_URL + "traces/";

// https://localhost:44301/#/product/trace/14cc2cae-3eb3-42a2-9ac8-662d943a1854
class ProductTrace extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: false,
      showQR: false,
      showAddedTrace: false,
      activeTab: "1",
      product: {
        nutritionInformation: {}
      },
      trace: {},
      waypoints: [
        {
          source: {},
          destination: {}
        }
      ],
      distances: {
        otherDistance: 0,
        truckDistance: 0,
        shipDistance: 0,
        airplaneDistance: 0,
        trainDistance: 0,
      },
      manufacturer: {
        Location: {}
      }
    };

    this.onShowQr = this.onShowQr.bind(this);
    this.toggleQR = this.toggleQR.bind(this);
    this.toggleAddedTrace = this.toggleAddedTrace.bind(this);
  }

  componentDidMount() {
    this.setupTrace()
      .catch(() => this.props.history.push("/404"));
    //document.addEventListener("keydown", this.toggleQR, false);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.toggleQR, false);
  }

  onShowQr() {
    this.setState({ showQR: true });
  }

  toggleQR() {
    this.setState(state => {
      return { showQR: !state.showQR };
    });
  }

  toggleAddedTrace() {
    this.setState(state => {
      return { showAddedTrace: !state.showAddedTrace };
    });
  }

  mapTrace(trace) {
    return new Promise(resolve => {
      let distances = {
        otherDistance: 0,
        truckDistance: 0,
        shipDistance: 0,
        airplaneDistance: 0,
        trainDistance: 0,
      };

      let waypoints = [];


      distances = trace.waypoints.reduce((accumulatedDistances, currentWaypoint) => {
        switch (currentWaypoint.transportType) {
          case 0:
            return {
              ...accumulatedDistances,
              otherDistance: accumulatedDistances.otherDistance + currentWaypoint.emission.distanceInKm
            };
          case 1:
            return {
              ...accumulatedDistances,
              truckDistance: accumulatedDistances.truckDistance + currentWaypoint.emission.distanceInKm
            };
          case 2:
            return {
              ...accumulatedDistances,
              shipDistance: accumulatedDistances.shipDistance + currentWaypoint.emission.distanceInKm
            };
          case 3:
            return {
              ...accumulatedDistances,
              airplaneDistance: accumulatedDistances.airplaneDistance + currentWaypoint.emission.distanceInKm
            };
          case 4:
            return {
              ...accumulatedDistances,
              trainDistance: accumulatedDistances.trainDistance + currentWaypoint.emission.distanceInKm
            };
          default:
            return accumulatedDistances;
        }
      }, distances);

      trace.waypoints.forEach(waypoint => waypoints.push({ source: waypoint.source, destination: waypoint.destination }));
      resolve({ distances: distances, waypoints: waypoints });
    });
  }

  async setupTrace() {
    let traceData = await fetch(TRACE_URL + this.props.match.params.traceId);
    let mappedTrace = await this.mapTrace(traceData.trace);

    this.setState({
      product: traceData.product,
      trace: traceData.trace,
      distances: mappedTrace.distances,
      waypoints: mappedTrace.waypoints,
      manufacturer: traceData.manufacturer,
      loaded: true
    });


  }

  parseDistance(distance) {
    return parseFloat(
      Math.round(distance * 100) / 100
    ).toFixed(2);
  }


  render() {
    const { totalEmissionsCoTwoInMg, quantityInG } = this.state.trace;
    const totalDistanceInKm = this.parseDistance(this.state.trace.totalDistanceInKm);
    const { Name } = this.state.manufacturer;

    const { otherDistance, truckDistance, shipDistance, airplaneDistance, trainDistance } = this.state.distances;

    const transport = (
      <Table responsive>
        <tbody>
          <TransportTableRow color="green" distance={this.parseDistance(otherDistance)} iconName="bicycle" />
          <TransportTableRow color="darkgreen" distance={this.parseDistance(truckDistance)} iconName="truck" />
          <TransportTableRow color="orange" distance={this.parseDistance(trainDistance)} iconName="train" />
          <TransportTableRow color="red" distance={this.parseDistance(shipDistance)} iconName="ship" />
          <TransportTableRow color="darkred" distance={this.parseDistance(airplaneDistance)} iconName="plane" />
        </tbody>
      </Table>
    );

    return (
      <div className="animated fadeIn">
        <Row>
          <CardBody>
            <Col xs="12" md="10">
              <Row>
                <Col xs="12" md="10">
                  <h2 className="mb-4">{this.state.product.name}</h2>


                </Col>
                <Col xs="12" md="2">
                  <Button
                    onClick={this.toggleAddedTrace}
                    color="success"
                    style={{ marginBottom: "1em" }}
                  >
                    Zu meinem Fussabdruck hinzufügen
              </Button>
                </Col>
              </Row>
              <Row>
                <Col xs="12" md="5">
                  <h4 className="mb-4">Hersteller: {Name}</h4>
                </Col>
              </Row>
            </Col>
          </CardBody>
        </Row>
        <Row>
          <Col xs="12" md="4" className="mb-4">
            <Card>
              <CardBody>
                <h4>Umweltdaten: </h4>

                <Table responsive striped>
                  <tbody>
                    <tr>
                      <td>Total Co2 Emissionen</td>
                      <td className="text-right">{totalEmissionsCoTwoInMg} mg</td>
                    </tr>
                    <tr>
                      <td>Distanz Total</td>
                      <td className="text-right">
                        {totalDistanceInKm}
                        Km
                      </td>
                    </tr>
                    <tr>
                      <td>Menge</td>
                      <td className="text-right">{quantityInG} Grams</td>
                    </tr>
                    <tr>
                      <td />
                      <td />
                    </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
          <Col xs="12" md="4">
            <Card>
              <CardBody>
                <h4>Verwendete Transportmittel:</h4>
                {transport}
              </CardBody>
            </Card>
          </Col>
          <Col xs="12" md="4" verticalAlign="center">
            <Card>
              <CardBody>

                <img src={this.state.product.imageUrl} height="200px"  alt={this.state.product.name}/>
              </CardBody>
            </Card>
          </Col>

        </Row >
        <Row>
          <Col>
            <Loader loaded={this.state.loaded} className="spinner">
              <ProductMap
                waypoints={this.state.waypoints}
                manufacturer={this.state.manufacturer}
              />
            </Loader>
          </Col>
        </Row>
        <QrCodeModal
          display={this.state.showQR}
          toggleFunction={this.toggleQR}
          traceId="14cc2cae-3eb3-42a2-9ac8-662d943a1854"
        />
        <AddedTraceModal
          display={this.state.showAddedTrace}
          toggleFunction={this.toggleAddedTrace}
        />
      </div >
    );
  }
}

export default withRouter(ProductTrace);
