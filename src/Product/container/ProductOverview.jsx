import React, { Component } from "react";
import { Row } from "reactstrap";
import { fetch } from "../../User/AuthService";
import ProductCard from "../components/ProductCard";

const API_URL = process.env.REACT_APP_API_URL;
const PRODUCTS_URL = API_URL + "products";

class ProductOverview extends Component {
  constructor(props) {
    super(props);
    this.state = { products: [] };
  }

  componentDidMount() {
    fetch(PRODUCTS_URL).then(products => this.setState({ products: products }));
  }

  render() {
    return (
      <Row>
        {this.state.products.map(product => (
          <ProductCard product={product} key={product.id} />
        ))}
      </Row>
    );
  }
}

export default ProductOverview;
