import React, { Component } from "react";
import { Card, CardBody, Col, Row, Table } from "reactstrap";
import { withRouter } from "react-router-dom";
import { fetch } from "../../User/AuthService";

import ProductMap from "../components/ProductMap";

const API_URL = process.env.REACT_APP_API_URL;
const PRODUCTS_URL = API_URL + "products/";

class ProductDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: "1",
      product: {
        nutritionInformation: {}
      }
    };
  }

  componentDidMount() {
    fetch(PRODUCTS_URL + this.props.match.params.productId)
      .then(product => this.setState({ product: product }))
      .catch(() => this.props.history.push("/404"));
  }

  render() {
    const {
      energyKcal,
      energyKj,
      fatG,
      saturatedFattyAcidsG,
      carbohydrateG,
      ofWhichSugarG,
      dietaryFiberG,
      proteinG,
      saltG
    } = this.state.product.nutritionInformation;
    return (
      <div className="animated fadeIn">
        <h2 className="mb-4">{this.state.product.name}</h2>
        <Row>
          <Card>
            <CardBody>
              <Col xs="12" md="12" className="mb-12">
                <Table responsive striped>
                  <tbody>
                    <tr>
                      <td>Energy</td>
                      <td className="text-right">
                        {energyKcal} kcal / {energyKj} kj
                  </td>
                    </tr>
                    <tr>
                      <td>Fat</td>
                      <td className="text-right">{fatG} g</td>
                    </tr>
                    <tr>
                      <td>Saturated Fatty Acids</td>
                      <td className="text-right">{saturatedFattyAcidsG} g</td>
                    </tr>
                    <tr>
                      <td>
                        <div>Carbohydrate</div>
                        <div>of which Sugar</div>
                      </td>
                      <td className="text-right">
                        <div>{carbohydrateG} g</div>
                        <div>{ofWhichSugarG} g</div>
                      </td>
                    </tr>
                    <tr>
                      <td>Dietary Fiber</td>
                      <td className="text-right">{dietaryFiberG} g</td>
                    </tr>
                    <tr>
                      <td>Protein</td>
                      <td className="text-right">{proteinG} g</td>
                    </tr>
                    <tr>
                      <td>Salt</td>
                      <td className="text-right">{saltG} g</td>
                    </tr>
                  </tbody>
                </Table>
              </Col>
            </CardBody>
          </Card>
          <Card>
            <CardBody>
              <Col xs="12" md="6">
                <img src={this.state.product.imageUrl} height="200px" style={{ marginBottom: "1em" }} />
              </Col>
            </CardBody>
          </Card>
        </Row >
      </div >
    );
  }
}

export default withRouter(ProductDetail);
