import React, { Component } from "react";
import DeckGL, { LineLayer, ScatterplotLayer, TextLayer } from "deck.gl";
import { StaticMap } from "react-map-gl";
// Set your mapbox access token here
const MAPBOX_ACCESS_TOKEN =
  "pk.eyJ1Ijoic2FuZHJvbGlsbyIsImEiOiJjam9lanppbXowM2xlM3ZtaG9vcDR4ZGdzIn0.XCjA4FyrJIVFaDd5hnWAQA";
const dotRadius = 5;
const dotColour = [0, 255, 0];
const lineColour = [0, 255, 0];
const textColour = [0, 255, 0];


class ProductMap extends Component {
  constructor(props) {
    super(props);
  }

  createMapData(waypoints, origin) {
    let scatterPlotData = {
      id: "scatterplot-layer",
      data: [],
      radiusScale: 250
    };
    let lineData = {
      id: "line-layer",
      data: [],
      strokeWidth: 3,
      color: lineColour,
      getSourcePosition: d => d.from.coordinates,
      getTargetPosition: d => d.to.coordinates,
    };
    let textData = {
      size: 5,
      data: []
    };

    scatterPlotData.data.push({
      position: [origin.Location.Lon, origin.Location.Lat],
      radius: dotRadius,
      color: dotColour
    });
    textData.data.push(
      {
        position: [origin.Location.Lon, origin.Location.Lat + 0.01],
        text: origin.Name
      },
      {
        position: [waypoints[waypoints.length - 1].destination.Lon, waypoints[waypoints.length - 1].destination.Lat + 0.01],
        text: "Ziel"
      }
    );

    waypoints.forEach(waypoint => {
      scatterPlotData.data.push(
        {
          position: [waypoint.destination.Lon, waypoint.destination.Lat],
          radius: dotRadius,
          color: dotColour
        }
      );

      lineData.data.push({
        from: { coordinates: [waypoint.source.Lon, waypoint.source.Lat] },
        to: { coordinates: [waypoint.destination.Lon, waypoint.destination.Lat] }
      });
    });

    return [
      new ScatterplotLayer(scatterPlotData),
      new LineLayer(lineData),
      new TextLayer(textData)
    ];
  }

  render() {
    const manufacturer = this.props.manufacturer;
    const waypoints = this.props.waypoints;

    let initialViewState = {
      longitude: manufacturer.Location.Lon,
      latitude: manufacturer.Location.Lat,
      zoom: 9,
      pitch: 0,
      bearing: 0
    };

    return (
      <React.Fragment>
        <DeckGL
          height="400px"
          initialViewState={initialViewState}
          controller={true}
          layers={this.createMapData(waypoints, manufacturer)}
        >
          <StaticMap mapStyle="mapbox://sprites/mapbox/streets-v9" mapboxApiAccessToken={MAPBOX_ACCESS_TOKEN} s/>
        </DeckGL>
      </React.Fragment>
    );
  }
}

export default ProductMap;
