import React from "react";
import { Card, CardBody, Col } from "reactstrap";
import { withRouter } from "react-router-dom";

const imgStyle = {
  height: "100px"
};

const inlineBlock = {
  display: "inline-block",
  margin: "1em"
};

const ProductCard = withRouter(({ history, product }) => (
  <Col xs="12" md="4" className="mb-4">
    <Card
      onClick={() => {
        history.push("/product/" + product.id);
      }}
    >
      <CardBody className="clearfix p-3">
        <div style={inlineBlock} className="col-2">
          <img src={product.imageUrl} style={imgStyle} />
        </div>
        <div style={inlineBlock} className="col-8">
          <div className="h5 mb-0 text-info">{product.name}</div>
          <div className="text-muted text-uppercase font-weight-bold font-xs">
            {product.pricePerKg} CHF / kg
        </div>
        </div>
      </CardBody>
    </Card>
  </Col>
));

export default ProductCard;
